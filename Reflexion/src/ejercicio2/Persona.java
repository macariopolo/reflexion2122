package ejercicio2;

import java.util.ArrayList;

import org.json.JSONObject;

import anotaciones.Ejecutor;
import anotaciones.Excluido;
import anotaciones.JSONable;
import anotaciones.JSONeador;

@JSONable
public class Persona {
	private int id;
	private String nombre;
	@Excluido
	private String pwd;
	@Excluido
	private Persona padre, madre;
	private ArrayList<Persona> hijos = new ArrayList<>();
	
	public void print(int n, boolean mayusculas) {
		for (int i=0; i<n; i++)
			this.print(mayusculas);
	}
	
	public void print() {
		System.out.println(this.nombre);
	}
	
	public void print(boolean mayusculas) {
		if (mayusculas)
			System.out.println(this.nombre.toUpperCase());
		else
			System.out.println(this.nombre.toLowerCase());
	}

	public static void main(String[] args) throws Exception {
		Persona putin = new Persona();
		putin.id = 25;
		putin.nombre = "Putin";
		putin.pwd = "Putin es un puto123.";
		
		Persona katerina = new Persona();
		katerina.id = 40;
		katerina.nombre = "Katerina";
		katerina.pwd = "1234";
		katerina.padre = putin;
		
		Persona putina = new Persona();
		putina.id = 80;
		putina.nombre = "Putina";
		putina.pwd = "4567";
		putina.padre = putin;
		
		putin.hijos.add(katerina);
		putin.hijos.add(putina);
		
		JSONObject jso = JSONeador.toJSON(putin);
		System.out.println(jso);
		
		Ejecutor.ejecutar(putin);
	}
}
