package anotaciones;

import java.lang.reflect.Field;
import java.util.Collection;
import java.util.Iterator;

import org.json.JSONArray;
import org.json.JSONObject;

public class JSONeador {
	
	public static JSONObject toJSON(Object o) throws Exception {
		Class<?> clazz = o.getClass();
		if (!clazz.isAnnotationPresent(JSONable.class))
			throw new Exception("Falta la anotación JSONable");
		
		JSONObject jso = new JSONObject();
		Field[] fields = clazz.getDeclaredFields();
		
		for (Field field : fields) {
			field.setAccessible(true);
			if (field.isAnnotationPresent(Excluido.class))
				continue;
			
			String name = field.getName();
			Object value = field.get(o);
			
			if (field.getType().isPrimitive() || field.getType()==String.class) {
				jso.put(name, value);
			} else if (field.getType().isAnnotationPresent(JSONable.class)) {
				if (value!=null)
					jso.put(name, JSONeador.toJSON(value));
			} else if (Collection.class.isAssignableFrom(field.getType())) {
				JSONArray jsa = new JSONArray();
				Collection<?> col = (Collection<?>) value;
				Iterator<?> iCol = col.iterator();
				while (iCol.hasNext())
					jsa.put(toJSON(iCol.next()));
				jso.put(name, jsa);
			}
		}
		return jso;
	}
}
