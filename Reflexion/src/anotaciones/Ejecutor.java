package anotaciones;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

public class Ejecutor {

	public static void ejecutar(Object o) throws Exception {
		Class<?> clazz = o.getClass();
		Method[] methods = clazz.getDeclaredMethods();
		
		for (Method method : methods) {
			method.setAccessible(true);
			if (!Modifier.isStatic(method.getModifiers()))
				method.invoke(o, getRandomValues(method.getParameterTypes()));
		}
	}

	private static Object[] getRandomValues(Class<?>[] parameterTypes) {
		Object[] result = new Object[parameterTypes.length];
		for (int i=0; i<parameterTypes.length; i++) {
			result[i] = getRandomValue(parameterTypes[i]);
		}
		return result;
	}

	private static Object getRandomValue(Class<?> clazz) {
		if (clazz==Integer.class || clazz==int.class)
			return 5;
		if (clazz==Float.class || clazz==Double.class || 
				clazz==float.class || clazz==double.class )
			return 5.0;
		if (clazz==Boolean.class || clazz==boolean.class)
			return true;
		return null;
	}
	
}
