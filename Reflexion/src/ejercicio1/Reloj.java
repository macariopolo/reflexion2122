package ejercicio1;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;

public class Reloj {
	private static ArrayList<Field> loadFields(Object o) {
		ArrayList<Field> result = new ArrayList<>();
		Field[] fields = o.getClass().getDeclaredFields();
		for (Field field : fields)
			result.add(field);
		if (o.getClass().getSuperclass()!=null) {
			fields = o.getClass().getSuperclass().getDeclaredFields();
			for (Field field : fields)
				result.add(field);
		}
		return result;
	}
	
	private static void showCamposPrimitivos(Object o) throws Exception {
		Class clazz = o.getClass();
		ArrayList<Field> fields = loadFields(o);
		for (Field field : fields) {
			field.setAccessible(true);
			if (field.getType().isPrimitive()) {
				System.out.println(field.getName() + " = " + field.get(o));
			}
		}
		System.out.println("-----------");
	}
	
	public static void main(String[] args) throws Exception {
		Fichero f = new A();
		showCamposPrimitivos(f);
	}

	public static void main2(String[] args) throws Exception {
		char extension = 'C';
		
		Class clazz = Class.forName("ejercicio1." + extension);
		Fichero f = (Fichero) clazz.newInstance();
		
		Field[] fields = clazz.getDeclaredFields();
		for (Field field : fields) {
			System.out.println(field.getName());
		}
		
		Method[] methods = clazz.getDeclaredMethods();
		for (Method method : methods) {
			if (method.getParameterCount()==1) {
				Class<?>[] types = method.getParameterTypes();
				if (types[0]==String.class) {
					// f.setCorreo("prueba");
					try {
						method.invoke(f, "prueba");
					} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		}
		
		for (Field field : fields) {
			try {
				field.setAccessible(true);
				System.out.println(field.get(f));
			} catch (IllegalArgumentException | IllegalAccessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

}
